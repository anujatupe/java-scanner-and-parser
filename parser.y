%{
#include <stdio.h>
int lineNum = 1;
%}

%token IDENTIFIER
%token IMPORT
%token PACKAGE
%token COMMENT
%token CLASS
%token INTERFACE
%token EXTENDS
%token MODIFIER
%token IMPLEMENTS
%token TYPESPECIFIER
%token VOID
%token STATEMENT
%token PLUS_SHORTHAND MINUS_SHORTHAND TIMES_SHORTHAND DIVIDE_SHORTHAND MODULO_SHORTHAND INCR_OP DECR_OP
%token INTEGER
%token NOTEQUAL LESSTHANEQUAL GREATERTHANEQUAL
%token STATIC
%token IF ELSE WHILE FOR DO
%token RETURN
%token NULL_KEYWORD SUPER THIS
%token EXPRESSION
%token UMINUS
%token AND
%token OROR ANDAND
%token TRUE FALSE
%token TRIPLE_SHIFT_RIGHT DOUBLE_SHIFT_RIGHT SHIFT_LEFT
%token OR_SHORTHAND AND_SHORTHAND XOR_SHORTHAND SHIFT_LEFT_SHORTHAND TRIPLE_SHIFT_RIGHT_SHORTHAND DOUBLE_SHIFT_RIGHT_SHORTHAND
%token INSTANCEOF
%token CASE DEFAULT SWITCH
%token BREAK CONTINUE
%token SYNCHRONIZED
%token TRY THROWS THROW CATCH FINALLY
%token NEW
%token FLOAT_NUMBER
%token STRING_LITERAL

%nonassoc '=' PLUS_SHORTHAND MINUS_SHORTHAND TIMES_SHORTHAND DIVIDE_SHORTHAND MODULO_SHORTHAND AND_SHORTHAND OR_SHORTHAND XOR_SHORTHAND SHIFT_LEFT_SHORTHAND TRIPLE_SHIFT_RIGHT_SHORTHAND DOUBLE_SHIFT_RIGHT_SHORTHAND

/*%left '='*/
%left OROR
%left ANDAND
%left '|'
%left '^'
%left '&'
%left EQUALEQUAL NOTEQUAL
%left '<' '>' LESSTHANEQUAL GREATERTHANEQUAL
%left TRIPLE_SHIFT_RIGHT DOUBLE_SHIFT_RIGHT SHIFT_LEFT
%left '+' '-'
%left '*' '/' '%'
%right UMINUS '~' '!'
%left INCR_OP DECR_OP
%left '[' ']'

%start compilation_unit

%%
  
compilation_unit
	: package_statements import_statements type_declarations 
	| import_statements type_declarations
	| package_statements type_declarations
	| type_declarations
	;

import_statements
	: import_statements import_statement
	| import_statement
	;

package_statements
	: package_statement
	;
	
package_statement
	: PACKAGE package_name ';'
	;
	
import_statement
	: IMPORT package_name '.' '*' ';'
	| IMPORT class_or_interface_name ';'
	;

type_declarations
	: type_declarations type_declaration
	| type_declaration
	;

type_declaration
	: class_declaration ';'
	| class_declaration
	| interface_declaration ';'
	| interface_declaration
	;

class_declaration
	: modifier class_body
	| class_body
	;

class_body
	: CLASS IDENTIFIER EXTENDS class_or_interface_name IMPLEMENTS implements '{' field_declarations '}'
        | CLASS IDENTIFIER EXTENDS class_or_interface_name '{' field_declarations  '}'
        | CLASS IDENTIFIER IMPLEMENTS implements '{'field_declarations '}'
        | CLASS IDENTIFIER '{' field_declarations '}'
        ;

implements
	: implements ',' class_or_interface_name
	| class_or_interface_name

interface_declaration 
	: modifier INTERFACE IDENTIFIER EXTENDS class_or_interface_name '{' field_declarations '}'
	| modifier INTERFACE IDENTIFIER '{' field_declarations '}'
	;

field_declarations
	: field_declarations field_declaration
	| field_declaration
	;

field_declaration
	: method_declaration ';'
	| method_declaration
	| constructor_declaration
	| variable_declaration
	| static_initializer
	;

method_declaration
	: modifier method_body
	| method_body
	;

method_body
	: type IDENTIFIER '(' ')' THROWS class_or_interface_name statement_block
        | type IDENTIFIER '(' parameter_list ')' THROWS class_or_interface_name statement_block
        | VOID IDENTIFIER '(' ')' THROWS class_or_interface_name statement_block
        | VOID IDENTIFIER '(' parameter_list ')' THROWS class_or_interface_name statement_block
        | type IDENTIFIER '(' ')' statement_block
        | type IDENTIFIER '(' parameter_list ')' statement_block
        | VOID IDENTIFIER '(' ')' statement_block
        | VOID IDENTIFIER '(' parameter_list ')' statement_block
	;

constructor_declaration
	: modifier constructor_body 
	| constructor_body
	;

constructor_body
	: IDENTIFIER '(' ')' statement_block
        | IDENTIFIER '(' parameter_list ')' statement_block
	;

variable_declaration
	: modifier type variable_declarators ';'
	| type variable_declarators ';'
	;

static_initializer 
	: MODIFIER statement_block 
	;

variable_declarators
	: variable_declarators ',' variable_declarator
	| variable_declarator
	;

variable_declarator
	: IDENTIFIER multi_dimension_brackets '=' variable_initializers
	| IDENTIFIER multi_dimension_brackets
	| IDENTIFIER '=' variable_initializers
	| IDENTIFIER
	;

variable_initializers
        : '{' variable_initializers '}'
        | variable_initializers ',' variable_initializers
	| expressions
        ;

expressions
	: expressions ',' expression
	| expression
	;

expression
	: assignment_expression
	| creating_expression
	;

creating_expression
	: NEW class_or_interface_name '(' expressions ')'
	| NEW '(' expression ')'
	| NEW class_or_interface_name '(' ')'
	| NEW TYPESPECIFIER multi_dimensional_arrays multi_dimension_brackets
	| NEW TYPESPECIFIER multi_dimensional_arrays
	| NEW TYPESPECIFIER multi_dimension_brackets
	| NEW class_or_interface_name multi_dimensional_arrays multi_dimension_brackets
	| NEW class_or_interface_name multi_dimensional_arrays
	| NEW class_or_interface_name multi_dimension_brackets
	;

multi_dimensional_arrays
	: multi_dimensional_arrays multi_dimensional_array
	| multi_dimensional_array
	;

multi_dimensional_array
	: '[' expression ']'
	;

multi_dimension_brackets
	: multi_dimension_brackets multi_dimension_bracket
	| multi_dimension_bracket
	;

multi_dimension_bracket
	: '[' ']'
	;

assignment_expression
	: assignment_expression assignment_operators ternary_expression
	| assignment_expression assignment_operators creating_expression
	| ternary_expression
	;

assignment_operators
	: '='
	| PLUS_SHORTHAND
	| MINUS_SHORTHAND
	| TIMES_SHORTHAND
	| DIVIDE_SHORTHAND
	| MODULO_SHORTHAND
	| AND_SHORTHAND
	| OR_SHORTHAND
	| XOR_SHORTHAND
	| SHIFT_LEFT_SHORTHAND
	| TRIPLE_SHIFT_RIGHT_SHORTHAND
	| DOUBLE_SHIFT_RIGHT_SHORTHAND
	; 



ternary_expression
	: ternary_expression '?' l_expression ':' l_expression
	| l_expression
	;

l_expression 
	: l_expression OROR l_term
	| l_term

l_term
	: l_term ANDAND b_expression
	| b_expression
	;


b_expression
        : b_expression '|' b_term
	| b_expression '^' b_term
        | b_term
        ;

b_term
        : b_term '&' relation
        | relation
        ;

relation
        : relation relop shift_expression
        | relation INSTANCEOF class_or_interface_name
	| shift_expression
        ;

relop
        : '>'
        | '<'
        | EQUALEQUAL
        | NOTEQUAL
        | LESSTHANEQUAL
        | GREATERTHANEQUAL
        ;

shift_expression
	: shift_expression TRIPLE_SHIFT_RIGHT numeric_expression
	| shift_expression DOUBLE_SHIFT_RIGHT numeric_expression
	| shift_expression SHIFT_LEFT numeric_expression
	| numeric_expression
	;

numeric_expression
	: numeric_expression '+' term
	| numeric_expression '-' term
	| term
	;  

term
	: term '*' prefix_incr_decr
	| term '/' prefix_incr_decr
	| term '%' prefix_incr_decr
	| prefix_incr_decr
	;

prefix_incr_decr
	: INCR_OP postfix_incr_decr
	| DECR_OP postfix_incr_decr
	| '-' factor %prec UMINUS
       	| '~' factor 
        | '!' factor
	| postfix_incr_decr 
	;

postfix_incr_decr
	: factor INCR_OP
	| factor DECR_OP
	| factor
	;

factor
	: '(' expression ')'
	| '(' TYPESPECIFIER ')' factor
	| '(' class_or_interface_name ')' factor
	| function_calls
	| id 
	;

function_calls 
	: function_calls '.' function_call
	| function_call
	;

function_call
	: class_or_interface_name '(' expressions ')'
        | class_or_interface_name '(' ')'
	| SUPER '(' expressions ')'
	| SUPER '(' ')'
	;

id 
	: class_or_interface_name multi_dimensional_arrays
	| class_or_interface_name
	| literal_expression
	| NULL_KEYWORD
	| SUPER
	| THIS
	| TRUE
        | FALSE
	;

literal_expression
	: INTEGER
	| FLOAT_NUMBER	
	| STRING_LITERAL
	;

parameter_list
	: parameter_list ',' parameter
	| parameter
	;

parameter
	: type IDENTIFIER multi_dimension_brackets
	| type IDENTIFIER
	;

type
	: TYPESPECIFIER multi_dimension_brackets
	| TYPESPECIFIER 
	| class_or_interface_name multi_dimension_brackets
	| class_or_interface_name
	;

statement_block
	: '{' statements '}'
	| '{' '}'
	;

statements
	: statements statement
	| statement
	;

statement
	: STATEMENT
	| variable_declaration
	| expression ';'
	| statement_block 
	| if_statement
	| do_statement
	| while_statement
	| for_statement
	| return_statements
	| switch_statement
	| try_statement
	| SYNCHRONIZED '(' expression ')' statement
	| THROW expression ';'
	| break_statement
	| continue_statement
	| IDENTIFIER ':' statement
	| ';'
	;

return_statements
	: RETURN expression ';'
	| RETURN ';'
	;

if_statement
	: IF '(' expression ')' statement
	| IF '(' expression ')' statement ELSE statement
	;

do_statement
	: DO statement WHILE '(' expression ')' ';'
	;

while_statement
	: WHILE '(' expression ')' statement
	;

for_statement
	: FOR '(' variable_declaration for_expression for_expression  ')' statement
	| FOR '(' for_expression for_expression for_expression ')' statement
	| FOR '(' variable_declaration for_expression expressions  ')' statement
	| FOR '(' for_expression for_expression expressions ')' statement
	| FOR '(' variable_declaration for_expression  ')' statement
	| FOR '(' for_expression for_expression ')' statement
	;

for_expression
	: expressions ';'
	| ';'
	;

switch_statement
	: SWITCH '(' expression ')' '{' cases '}'
	;

cases
	: cases case
	| case
	;

case 
	: CASE expression ':' statement break_statement
	| CASE expression ':' statement
	| CASE expression ':' 
	| DEFAULT ':' statement break_statement
	| DEFAULT ':' statement
	| DEFAULT ':'
	;

try_statement
	: TRY statement catch_statements finally_statement
	;

catch_statements
	: catch_statements catch_statement 
	| catch_statement
	|
	;

catch_statement
	: CATCH '(' parameter ')' statement
	;

finally_statement
	: FINALLY statement
	|
	;

break_statement
	: BREAK IDENTIFIER ';'
	| BREAK ';'
	;

continue_statement
	: CONTINUE IDENTIFIER ';'
	| CONTINUE ';'
	;

modifier
	: modifier modifiers
        | modifiers

modifiers
	: MODIFIER
        | SYNCHRONIZED
	;

package_name
	: package_name '.' IDENTIFIER
	| IDENTIFIER
	;
	
class_or_interface_name
	: package_name '.' IDENTIFIER
	| IDENTIFIER
	;

%%

int yyerror(char *s) {
  printf(" %s\n",s);
}

int main(void) {
  yyparse();
}
