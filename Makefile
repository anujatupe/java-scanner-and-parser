parser: parser.tab.o lex.yy.o
	gcc -g -o P1 lex.yy.o parser.tab.o

lex.yy.o: parser.l
	flex parser.l; gcc -c lex.yy.c


parser.tab.o: parser.y
	bison -d --debug --verbose parser.y; gcc -g -c parser.tab.c


#parser.tab.o: parser.y
#	bison -d parser.y; gcc -g -c parser.tab.c
clean:
	rm lex.yy.* parser.tab.* parser.output
