%{
	#include "parser.tab.h"
	int lineno = 1;
%}
%%
"import" { printf("\n%s \t line %d", yytext, lineno); return IMPORT; } 
"package" { printf("\n%s \t line %d", yytext, lineno); return PACKAGE; }
"class" { printf("\n%s \t line %d", yytext, lineno); return CLASS; }
"extends" { printf("\n%s \t line %d", yytext, lineno); return EXTENDS; }
"interface" { printf("\n%s \t line %d", yytext, lineno); return INTERFACE; }
"implements" {printf("\n%s \t line %d", yytext, lineno); return IMPLEMENTS; }
"finally" {printf("\n%s \t line %d", yytext, lineno); return FINALLY;}
"public"|"private"|"protected"|"static"|"final"|"native"|"abstract"|"threadsafe"|"transient" {
	printf("\n%s \t line %d", yytext, lineno); return MODIFIER;
}
"synchronized" {printf("\n%s \t line %d", yytext, lineno); return SYNCHRONIZED;}
"case" {printf("\n%s \t line %d", yytext, lineno); return CASE;}
"default" {printf("\n%s \t line %d", yytext, lineno); return DEFAULT;}
"switch" {printf("\n%s \t line %d", yytext, lineno); return SWITCH;}
"break" {printf("\n%s \t line %d", yytext, lineno); return BREAK;}
"continue" {printf("\n%s \t line %d", yytext, lineno); return CONTINUE;}
"void" {printf("\n%s \t line %d", yytext, lineno); return VOID;}
"boolean"|"byte"|"char"|"short"|"int"|"float"|"long"|"double" { printf("\n%s \t line %d", yytext, lineno); return TYPESPECIFIER; }
"statement" {printf("\n%s \t line %d", yytext, lineno); return STATEMENT; }
"if" {printf("\n%s \t line %d", yytext, lineno); return IF;}
"else" {printf("\n%s \t line %d", yytext, lineno); return ELSE;}
"while" {printf("\n%s \t line %d", yytext, lineno); return WHILE;}
"for" {printf("\n%s \t line %d", yytext, lineno); return FOR;}
"do" {printf("\n%s \t line %d", yytext, lineno); return DO;}
"try" {printf("\n%s \t line %d", yytext, lineno); return TRY;}
"throws" {printf("\n%s \t line %d", yytext, lineno); return THROWS;}
"throw" {printf("\n%s \t line %d", yytext, lineno); return THROW;}
"catch" {printf("\n%s \t line %d", yytext, lineno); return CATCH;}
"return" {printf("\n%s \t line %d", yytext, lineno); return RETURN;}
"null" {printf("\n%s \t line %d", yytext, lineno); return NULL_KEYWORD;}
"super" {printf("\n%s \t line %d", yytext, lineno); return SUPER;}
"this" {printf("\n%s \t line %d", yytext, lineno); return THIS;}
"true" {printf("\n%s \t line %d", yytext, lineno); return TRUE;}
"false" {printf("\n%s \t line %d", yytext, lineno); return FALSE;}
"instanceof" {printf("\n%s \t line %d", yytext, lineno); return INSTANCEOF;}
"new" {printf("\n%s \t line %d", yytext, lineno); return NEW;}
[_$a-zA-Z][_$a-zA-Z0-9]* { 
			 	//yylval.sval = malloc(strlen(yytext));
				//strncpy(yylval.sval, yytext, strlen(yytext));
				printf("\n%s \t line %d", yytext, lineno);
				return IDENTIFIER; 
			}
-?[0-9]+ { printf("\n%s \t line %d", yytext, lineno); return INTEGER; }
-?[0-9]+"."[0-9]* | 
-?"."[0-9]+ |
-?[0-9]+E[-+]?[0-9]+ |
-?[0-9]+"."[0-9]*E[-+]?[0-9]+ |
-?"."[0-9]+E[-+]?[0-9]+ {printf("\n%s \t line %d", yytext, lineno); return FLOAT_NUMBER; }
'(\\.|''|[^'\n])*' |
\"(\\.|\"\"|[^"\n])*\" {printf("\n%s \t line %d", yytext, lineno); return STRING_LITERAL; }
"\"" { printf("\n%s \t line %d", yytext, lineno); return '"'; }
"." { printf("\n%s \t line %d", yytext, lineno); return '.'; }
"," { printf("\n%s \t line %d", yytext, lineno); return ','; }
"*" { printf("\n%s \t line %d", yytext, lineno); return '*'; }
";" { printf("\n%s \t line %d", yytext, lineno); return ';'; }
"}" { printf("\n%s \t line %d", yytext, lineno); return '}'; }
"{" { printf("\n%s \t line %d", yytext, lineno); return '{'; }
"(" { printf("\n%s \t line %d", yytext, lineno); return '('; }
")" { printf("\n%s \t line %d", yytext, lineno); return ')'; }
"[" { printf("\n%s \t line %d", yytext, lineno); return '['; }
"]" { printf("\n%s \t line %d", yytext, lineno); return ']'; }
"+" { printf("\n%s \t line %d", yytext, lineno); return '+'; }
"-" { printf("\n%s \t line %d", yytext, lineno); return '-'; }
"/" { printf("\n%s \t line %d", yytext, lineno); return '/'; }
"%" { printf("\n%s \t line %d", yytext, lineno); return '%'; }
"=" { printf("\n%s \t line %d", yytext, lineno); return '='; }
"?" { printf("\n%s \t line %d", yytext, lineno); return '?'; }
":" { printf("\n%s \t line %d", yytext, lineno); return ':'; }
"&" { printf("\n%s \t line %d", yytext, lineno); return '&'; }
"|" { printf("\n%s \t line %d", yytext, lineno); return '|'; }
"!" { printf("\n%s \t line %d", yytext, lineno); return '!'; }
"^" { printf("\n%s \t line %d", yytext, lineno); return '^'; }
"~" { printf("\n%s \t line %d", yytext, lineno); return '~'; }
"<<" { printf("\n%s \t line %d", yytext, lineno); return SHIFT_LEFT; }
">>" { printf("\n%s \t line %d", yytext, lineno); return DOUBLE_SHIFT_RIGHT; }
">>>" { printf("\n%s \t line %d", yytext, lineno); return TRIPLE_SHIFT_RIGHT; }
"&&" { printf("\n%s \t line %d", yytext, lineno); return ANDAND; }
"||" { printf("\n%s \t line %d", yytext, lineno); return OROR; }
"+=" { printf("\n%s \t line %d", yytext, lineno); return PLUS_SHORTHAND; }
"-=" { printf("\n%s \t line %d", yytext, lineno); return MINUS_SHORTHAND; }
"*=" { printf("\n%s \t line %d", yytext, lineno); return TIMES_SHORTHAND; }
"/=" { printf("\n%s \t line %d", yytext, lineno); return DIVIDE_SHORTHAND; }
"%=" { printf("\n%s \t line %d", yytext, lineno); return MODULO_SHORTHAND; }
"&=" { printf("\n%s \t line %d", yytext, lineno); return AND_SHORTHAND; }
"|=" { printf("\n%s \t line %d", yytext, lineno); return OR_SHORTHAND; }
"^=" { printf("\n%s \t line %d", yytext, lineno); return XOR_SHORTHAND; }
">>=" { printf("\n%s \t line %d", yytext, lineno); return DOUBLE_SHIFT_RIGHT_SHORTHAND; }
">>>=" { printf("\n%s \t line %d", yytext, lineno); return TRIPLE_SHIFT_RIGHT_SHORTHAND; }
"<<=" { printf("\n%s \t line %d", yytext, lineno); return SHIFT_LEFT_SHORTHAND; }
"++" { printf("\n%s \t line %d", yytext, lineno); return INCR_OP; }
"--" { printf("\n%s \t line %d", yytext, lineno); return DECR_OP; }
"==" { printf("\n%s \t line %d", yytext, lineno); return EQUALEQUAL; }
"!=" { printf("\n%s \t line %d", yytext, lineno); return NOTEQUAL; }
"<=" { printf("\n%s \t line %d", yytext, lineno); return LESSTHANEQUAL; }
">=" { printf("\n%s \t line %d", yytext, lineno); return GREATERTHANEQUAL; }
"<" { printf("\n%s \t line %d", yytext, lineno); return '<'; }
">" { printf("\n%s \t line %d", yytext, lineno); return '>'; }
[ \t]+ { /* Ignore all the whitespaces */ }
\n { lineno++; }
%%

int yywrap(){
  return 1;
}
